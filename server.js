const express = require('express');
const app = express();

//serve static file on the server
app.use('/static',express.static('static'));

app.get('/', function(req,res) {
  res.sendFile(__dirname+'/index.html');
})

app.listen(4040, function(){
  console.log('Example app listening on port 4040!')
});
